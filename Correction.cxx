#ifndef CORRECTION_CXX
#define CORRECTION_CXX

#include "Correction.h"

/*
* la posibilite de renvoyer des corrections nominales, variees up, ou variee down;
* une fonction qui renvoie le numero de bin correpondant au eta de l'electron;
* une fonction qui renvoye la matrice de covariance entre les bins (cette derniere chose n'est pas si urgente, on peut demarrer sans);
*/

Correction::Correction() { }
Correction::~Correction() { }

// Lire les hists d'Alpha, C : /sps/atlas/h/hatmani/Test/results/SF_Low-mu.root : 

void Correction::initialize(TString File) {
  m_random = new TRandom3();
  m_file = TFile::Open(File); if(m_file==NULL){   std::cout<<"Electron calibration file "<< File << " not found!" << std::endl;  exit(10); }

  m_alpha = (TH1D *)m_file->Get("measScale_alpha");
  m_cterm = (TH1D *)m_file->Get("measScale_c");

  if(m_alpha==NULL || m_cterm==NULL){ std::cout<<"Electron calibration histograms not found!" << std::endl; exit(10);}  return;}


// Lire les matrix de covariances pour Alpha et C :
// Matrix de covariance pour alpha : /sps/atlas/h/hatmani/Test/results/Conv_Alpha.root  => TH2D : H2DCoV 
// Matrix de covariance pour   C   : /sps/atlas/h/hatmani/Test/results/Conv_C.root      => TH2D : H2DCoV 

void Correction::CoVarianceMatrixAlpha(TString FileCoVAlpha) {
     m_fileAlphaCovAlpha     = TFile::Open(FileCoVAlpha); if(m_fileAlphaCovAlpha==NULL){   std::cout<<"Electron calibration file "<< FileCoVAlpha << " not found!" << std::endl;  exit(10); }
     m_AlphaCoVAlpha         = (TH2D *)m_fileAlphaCovAlpha->Get("H2DCoV"); }

//  la matrice de covariance entre les bins :

void Correction::CoVarianceMatrixC(TString FileCoVC) {
     m_fileAlphaCovC         = TFile::Open(FileCoVC); if(m_fileAlphaCovAlpha==NULL){   std::cout<<"Electron calibration file "<< FileCoVC << " not found!" << std::endl;  exit(10); }
     m_AlphaCoVC             = (TH2D *)m_fileAlphaCovC->Get("H2DCoV"); }



// Get alpha/c nominal, avec +/- 1 sigma stat :

double Correction::getAlpha       (float eta)               { return m_alpha->GetBinContent(m_alpha->FindBin(eta));                                                      }
double Correction::getCTerm       (float eta)               { return m_cterm->GetBinContent(m_cterm->FindBin(eta));                                                      }

double Correction::getAlphaUp     (float eta)               { return m_alpha->GetBinContent(m_alpha->FindBin(eta))+m_alpha->GetBinError(m_alpha->FindBin(eta));          }
double Correction::getCTermUp     (float eta)               { return m_cterm->GetBinContent(m_cterm->FindBin(eta))+m_cterm->GetBinError(m_cterm->FindBin(eta));          }

double Correction::getAlphaDown   (float eta)               { return m_alpha->GetBinContent(m_alpha->FindBin(eta))-m_alpha->GetBinError(m_alpha->FindBin(eta));          }
double Correction::getCTermDown   (float eta)               { return m_cterm->GetBinContent(m_cterm->FindBin(eta))-m_cterm->GetBinError(m_cterm->FindBin(eta));          }


//  le numero de bin correpondant au eta de l'electron :

int Correction::BinNumberAlpha (float eta)               { return m_alpha->FindBin(eta);                                                                              }
int Correction::BinNumberC     (float eta)               { return m_cterm->FindBin(eta);                                                                              }

float  Correction::getAlphaCoV    (float eta1, float eta2)  { return m_AlphaCoVAlpha->GetBinContent(BinNumberAlpha(eta1),BinNumberAlpha(eta2));                          }
float  Correction::getCCoV        (float eta1, float eta2)  { return m_AlphaCoVC->GetBinContent(BinNumberC(eta1),BinNumberC(eta2));                                      }

// corrigerait le pT de l'electron par  +/- 1 sigma(alpha) stat :

float  Correction::getScaleCorrection(float pt, float eta, ScaleCorrectionType type) {

  float ptcorr;   float alpha = getAlpha(eta);
  if(type==DataRel)         ptcorr = pt / (1. + alpha);
  else if(type==DataAbs)    ptcorr = pt / alpha;
  else if(type==MCRel)      ptcorr = pt * (1. + alpha);
  else if(type==MCAbs)      ptcorr = pt * alpha;
  return ptcorr;
}

float  Correction::getScaleCorrectionUp(float pt, float eta, ScaleCorrectionType type) {

  float ptcorr;   float alpha = getAlphaUp(eta);
  if(type==DataRel)         ptcorr = pt / (1. + alpha);
  else if(type==DataAbs)    ptcorr = pt / alpha;
  else if(type==MCRel)      ptcorr = pt * (1. + alpha);
  else if(type==MCAbs)      ptcorr = pt * alpha;
  return ptcorr;
}

float  Correction::getScaleCorrectionDown(float pt, float eta, ScaleCorrectionType type) {

  float ptcorr;   float alpha = getAlphaDown(eta);
  if(type==DataRel)         ptcorr = pt / (1. + alpha);
  else if(type==DataAbs)    ptcorr = pt / alpha;
  else if(type==MCRel)      ptcorr = pt * (1. + alpha);
  else if(type==MCAbs)      ptcorr = pt * alpha;
  return ptcorr;
}


// corrigerait le pT de l'electron par  +/- 1 sigma(c) stat : 

float  Correction::getResolutionCorrection(float pt, float eta, ResolutionCorrectionType type) {

  float ptcorr = pt;   float c = getCTerm(eta);
  if(type==SamplingTerm)     ;
  else if(type==NoiseTerm)   ;
  else if(type==CstTerm)    ptcorr *= 1. + m_random->Gaus(0., c);
  return ptcorr;
}

float  Correction::getResolutionCorrectionUp(float pt, float eta, ResolutionCorrectionType type) {

  float ptcorr = pt;   float c = getCTermUp(eta);
  if(type==SamplingTerm)     ;
  else if(type==NoiseTerm)   ;
  else if(type==CstTerm)    ptcorr *= 1. + m_random->Gaus(0., c);
  return ptcorr;
}

float  Correction::getResolutionCorrectionDown(float pt, float eta, ResolutionCorrectionType type) {

  float ptcorr = pt;   float c = getCTermDown(eta);
  if(type==SamplingTerm)     ;
  else if(type==NoiseTerm)   ;
  else if(type==CstTerm)    ptcorr *= 1. + m_random->Gaus(0., c);
  return ptcorr;
}

void Correction::finalize() {  m_file->Close();  return;  }

#endif

