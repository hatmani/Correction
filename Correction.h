#ifndef CORRECTION_H
#define CORRECTION_H

#include <string>
#include <vector>
#include <cmath>
#include <iostream>

#include "TString.h"
#include "TMath.h"
#include <TH1.h>
#include <TH2.h>
#include <TFile.h>
#include <TRandom3.h>

class Correction {

 public:

  // the calibration factors alpha are either defined as Edata = (1+alpha) * Emc, or Edata = alpha * Emc, and stored accordingly
  // corrections should be applied accordingly
  enum ScaleCorrectionType {
    DataRel,    // E -> E / (1+alpha)
    DataAbs,    // E -> E / alpha
    MCRel,      // E -> E * (1+alpha)
    MCAbs       // E -> E * alpha
  };

  // allow for various resolution corrections. Not all are implemented
  enum ResolutionCorrectionType { SamplingTerm, NoiseTerm, CstTerm };

  Correction();
  virtual ~Correction();

  void  initialize(TString File);
  void  CoVarianceMatrixAlpha(TString File);
  void  CoVarianceMatrixC(TString File);

  void  finalize();

  double getAlpha(    float eta) ;
  double getAlphaUp(  float eta) ;
  double getAlphaDown(float eta) ;

  double getCTerm(   float eta);
  double getCTermUp( float eta);
  double getCTermDown(float eta);

  int BinNumberAlpha(float eta);
  int BinNumberC(float eta);

  float  getAlphaCoV(float eta1,float eta2);
  float  getCCoV(float eta1,float eta2);

  float  getResolutionCorrection(float pt, float eta, ResolutionCorrectionType type);
  float  getScaleCorrection(float pt, float eta, ScaleCorrectionType type);

  void setSeed(int seed) { m_random->SetSeed(seed); }

 private:

  TFile* m_file;
  TFile* m_fileAlphaCovAlpha;
  TFile* m_fileAlphaCovC;
  TH1D*  m_alpha;
  TH1D*  m_cterm;
  TH2D*  m_AlphaCoVAlpha;
  TH2D*  m_AlphaCoVC;

  TRandom3* m_random;

};

#endif
